CREATE DATABASE non_metallic_db;
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER ON non_metallic_db.* TO non_metallic_db@localhost IDENTIFIED BY 'non_metallic_db';
