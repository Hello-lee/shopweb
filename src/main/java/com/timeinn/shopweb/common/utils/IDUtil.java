package com.timeinn.shopweb.common.utils;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Copyright: Xinyang Green Creators Network Technology Co., Ltd
 * Author: lijing
 * Date: 2021/4/18
 * Description:
 */
public class IDUtil {
    private IDUtil() {
    }

    /**
     * 生成 32位 uuid
     *
     * @return
     */
    public static String generate() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 拼装ids
     *
     * @param idList idlist
     * @return
     */
    public static String concatIdsForMap(List<Map<String,String>> idList) {
        StringBuilder ids = new StringBuilder();
        for (Map<String,String> idMap : idList) {
            ids.append("'");
            ids.append(idMap.get("id"));
            ids.append("',");
        }
        return ids.substring(0, ids.length() - 1);
    }

    /**
     * 拼装ids
     *
     * @param idList idlist
     * @return
     */
    public static String concatIdsForString(List<String> idList) {
        StringBuilder ids = new StringBuilder();
        for (String id : idList) {
            ids.append("'");
            ids.append(id);
            ids.append("',");
        }
        return ids.substring(0, ids.length() - 1);
    }


    /**
     * 拼装ids
     *
     * @param idStr idlist
     * @return
     */
    public static String concatIdsForArr(String idStr) {
        String[] idArr = idStr.split(",");
        StringBuilder ids = new StringBuilder();
        for (String id : idArr) {
            ids.append("'");
            ids.append(id);
            ids.append("',");
        }
        return ids.substring(0, ids.length() - 1);
    }
}
