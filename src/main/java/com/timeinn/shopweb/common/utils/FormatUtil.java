package com.timeinn.shopweb.common.utils;

import java.math.BigDecimal;

/**
 * Copyright: Xinyang Green Creators Network Technology Co., Ltd
 * Author: lijing
 * Date: 2021/1/11
 * Description: 类型转换工具类
 */
public class FormatUtil {

    /**
     * Object to int
     *
     * @param i
     * @return
     */
    public static int toInt(Object i) {

        if (i == null) {
            return 0;
        }
        if (i instanceof Integer) {
            return ((Integer) i).intValue();
        } else if (i instanceof Short) {
            return ((Short) i).intValue();
        } else if (i instanceof BigDecimal) {
            return ((BigDecimal) i).intValue();
        } else if (i instanceof String) {
            try {
                return Integer.parseInt(((String) i));
            } catch (Exception e) {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Object to Str
     * @param str
     * @param def
     * @return
     */
    public static String toStr(Object str,String def){
        if(str==null){
            return def;
        }else if(str instanceof String){
            String s=((String)str).trim();
            return s;
        }else{
            return str.toString();
        }
    }

    /**
     * Object to float
     * @param i
     * @return
     */
    public static float toFloat(Object i){
        if(i==null){
            return (float)0.00;
        }else if(i instanceof Float){
            return (Float)i;
        }else if(i instanceof Integer){
            return (float)((Integer)i).floatValue();
        }else{
            return (float)0.00;
        }

    }

}
