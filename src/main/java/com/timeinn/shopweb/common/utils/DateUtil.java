package com.timeinn.shopweb.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author:lijing
 * Date:2019/6/25
 * Description: 日期转换工具类
 */
public class DateUtil {
    /**
     * 日期格式化输出
     *
     * @param date   日期
     * @param format 格式
     * @return
     */
    public static String formatDate(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }


    /**
     * 日期转时间戳
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static long timeToStamp(String dateStr, String format) {
        try {
            Date date = new Date();
            SimpleDateFormat sf = new SimpleDateFormat(format);
            date = sf.parse(dateStr);// 日期转换为时间戳
            return date.getTime();
        } catch (Exception e) {
            return 0;
        }
    }

}
