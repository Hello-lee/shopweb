package com.timeinn.shopweb.common.web;


import java.io.Serializable;

/**
 * Copyright: Xinyang Green Creators Network Technology Co., Ltd
 * Author: lijing
 * Date: 2021/4/25
 * Description: 结果集 状态码
 */
public class ActionResult implements Serializable {

    public enum ResultStatus {

        OK(0, "OK"),
        BIND_PHONE(201, "请绑定手机号"),
        PHONE_EXIT(202, "手机号码已存在"),
        //操作性失败
        FAILURE(301, "操作失败"),
        NEED_LOGIN(302, "需要登录"),
        NOT_PERMIT(302, "没有权限"),
        WRONG_CRE(303, "错误的凭证"),

        //第三方授权失败
        NO_OPENID(401, "获取授权失败"),
        NOT_UMTOKEN(402, "解析手机号失败"),

        SERVER_ERROR(500, "服务器异常");

        private int code;
        private String msg;

        ResultStatus(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
    }

    private int code;
    private String msg;
    private Object data;

    public ActionResult() {
        this(0, "OK", null);
    }

    public ActionResult(ResultStatus resultStatus) {
        this(resultStatus.code, resultStatus.msg, null);
    }

    public ActionResult(ResultStatus resultStatus, Object data) {
        this(resultStatus.code, resultStatus.msg, data);
    }

    public ActionResult(Object data) {
        this(0, "ok", data);
    }


    private ActionResult(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}

