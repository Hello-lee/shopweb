package com.timeinn.shopweb.common.web;

import com.timeinn.shopweb.project.syslogin.entity.SysUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Copyright: Xinyang Green Creators Network Technology Co., Ltd
 * Author: lijing
 * Date: 2021/5/14
 * Description: 往session中存储和获取信息
 */
public class SessionUtil {

    /**
     * 保存user
     *
     * @param request
     * @param sysUser
     */
    public static void saveLoginUser(HttpServletRequest request, SysUser sysUser) {
        HttpSession session = request.getSession();
        session.setAttribute("loginUser", sysUser);
    }

    /**
     * 读取user
     *
     * @param request
     * @return
     */
    public static SysUser getLoginUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        SysUser sysUser = (SysUser) session.getAttribute("loginUser");
        return sysUser;
    }

    /**
     * 登出
     *
     * @param request
     */
    public static void logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("loginUser", null);
    }

}
