package com.timeinn.shopweb.project.syslogin.mapper;

import com.timeinn.shopweb.project.syslogin.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface SysLoginMapper {
    SysUser sellerLogin(String userPhone, String password);
    SysUser clientLogin(@Param("userPhone") String userPhone, @Param("password") String password);
    int sellerRegister(Map<String, String> map);

    int clientRegister(Map<String, String> map);
}