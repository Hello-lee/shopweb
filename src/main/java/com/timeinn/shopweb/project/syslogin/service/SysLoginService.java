package com.timeinn.shopweb.project.syslogin.service;

import com.timeinn.shopweb.project.syslogin.entity.SysUser;
import com.timeinn.shopweb.project.syslogin.mapper.SysLoginMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SysLoginService {
    @Autowired
    private SysLoginMapper mapper;

    public SysUser sellerLogin(String userPhone, String password){
        SysUser usrList = mapper.sellerLogin(userPhone, password);
        return usrList;
    }

    public SysUser clientLogin(String userPhone, String password){
        SysUser usrList = mapper.clientLogin(userPhone, password);
        return usrList;
    }

    public int sellerRegister(Map<String, String> map) {
        int state =  mapper.sellerRegister(map);
        return state;
    }

    public int clientRegister(Map<String, String> map) {
        int state =  mapper.clientRegister(map);
        return state;
    }
}