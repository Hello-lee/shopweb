package com.timeinn.shopweb.project.syslogin.controller;

import com.timeinn.shopweb.common.web.ActionResult;
import com.timeinn.shopweb.common.web.SessionUtil;
import com.timeinn.shopweb.project.syslogin.entity.SysUser;
import com.timeinn.shopweb.project.syslogin.service.SysLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shop/syslogin")
public class SysLoginController {

    @Autowired
    private SysLoginService service;

    /**
     * 商家登录
     *
     * @param request
     * @param userPhone
     * @param password
     * @return
     */
    @RequestMapping("/sellerLogin")
    public ActionResult sellerLogin(HttpServletRequest request, String userPhone, String password) {
        try {
            SysUser item = service.sellerLogin(userPhone, password);
            if (item != null) {
                SessionUtil.saveLoginUser(request, item);
                return new ActionResult(ActionResult.ResultStatus.OK);
            } else {
                return new ActionResult(ActionResult.ResultStatus.FAILURE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
        }
    }

    /**
     * 商家注册
     *
     * @param request
     * @param map
     * @return
     */
    @RequestMapping("/sellerRegister")
    public ActionResult sellerRegister(HttpServletRequest request, @RequestBody Map<String, String> map) {
        try {
            int state = service.sellerRegister(map);
            return new ActionResult(state);
        } catch (Exception e) {
            e.printStackTrace();
            return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
        }
    }

    /**
     * 商家注册
     *
     * @param request
     * @param map     {}
     * @return
     */
    @RequestMapping("/clientRegister")
    public ActionResult clientRegister(HttpServletRequest request, Map<String, String> map) {
        try {
            int state = service.clientRegister(map);
            return new ActionResult(state);
        } catch (Exception e) {
            e.printStackTrace();
            return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
        }
    }

    /**
     * 用户登录
     *
     * @param request
     * @param userPhone
     * @param password
     * @return
     */
    @RequestMapping("/clientLogin")
    public ActionResult clientLogin(HttpServletRequest request, String userPhone, String password) {
        try {
            SysUser item = service.clientLogin(userPhone, password);
            if (item != null) {
                SessionUtil.saveLoginUser(request, item);
                return new ActionResult(ActionResult.ResultStatus.OK);
            } else {
                return new ActionResult(ActionResult.ResultStatus.FAILURE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
        }
    }


    /**
     * 获取登录用户信息
     * @return
     */
    @RequestMapping("/getLoginUser")
    public ActionResult getLoginUser(HttpServletRequest request) {
        try {
            SysUser sysUser = SessionUtil.getLoginUser(request);
            if (sysUser!=null){
                return new ActionResult(ActionResult.ResultStatus.OK, sysUser);
            }else {
                return new ActionResult(ActionResult.ResultStatus.NEED_LOGIN, sysUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
        }

    }


    /**
     * 登出
     *
     * @param request
     * @return
     */
    @RequestMapping("/logout")
    public ActionResult logout(HttpServletRequest request) {
       try {
           SessionUtil.logout(request);
           return new ActionResult(ActionResult.ResultStatus.OK);
       }catch (Exception e){
           return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
       }
    }
}