package com.timeinn.shopweb.project.index.service;

import com.timeinn.shopweb.project.index.mapper.IndexMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
 * Copyright: Xinyang Green Creators Network Technology Co., Ltd
 * Author: lijing
 * Date: 2021/5/12
 * Description:
 */
@Service
public class IndexService {

    @Autowired
    private IndexMapper mapper;

    /**
     * 获取商品分类
     *
     * @return
     */
    public List getSubMenu() {
        List list = mapper.getSubMenu();
        return list;
    }

    /**
     * 获取商品分类
     *
     * @return
     */
    public List<Map<String, Object>> getGoodsTypes() {
        List<Map<String, String>> goodsTypeList = mapper.getGoodsTypes();
        List<Map<String, String>> commList = mapper.getCommList();
        List<Map<String, String>> advList = mapper.getAdList();
        List<Map<String, Object>> resultList = new ArrayList();
        for (Map<String, String> typeMap : goodsTypeList) {
            Map<String, Object> resultMap = new HashMap<>();
            String typeCode = typeMap.get("typeCode");
            resultMap.put("typeName", typeMap.get("typeName"));
            List commGoodsList = new ArrayList();
            for (Map<String, String> commMap : commList) {
                String parentCode = commMap.get("parentCode");
                if (typeCode.equals(parentCode))
                    commGoodsList.add(commMap);
            }
            resultMap.put("commGoods", commGoodsList);
            List adList = new ArrayList();
            for (Map<String, String> adMap : advList) {
                String adCode = adMap.get("typeCode");
                if (typeCode.equals(adCode))
                    adList.add(adMap);
            }
            resultMap.put("adList", adList);
            resultList.add(resultMap);
        }
        return resultList;
    }

}
