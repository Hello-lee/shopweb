package com.timeinn.shopweb.project.index.mapper;

import com.timeinn.shopweb.project.index.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Copyright: Xinyang Green Creators Network Technology Co., Ltd
 * Author: lijing
 * Date: 2021/5/12
 * Description:
 */
@Mapper
@Repository
public interface IndexMapper {

    /**
     * 获取菜单
     *
     * @return
     */
    List<SysMenu> getSubMenu();
    List<Map<String,String>> getGoodsTypes();
    List<Map<String,String>> getCommList();
    List<Map<String,String>> getAdList();

}
