package com.timeinn.shopweb.project.index.controller;

import com.timeinn.shopweb.common.web.ActionResult;
import com.timeinn.shopweb.common.web.SessionUtil;
import com.timeinn.shopweb.project.index.service.IndexService;
import com.timeinn.shopweb.project.syslogin.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Copyright: Xinyang Green Creators Network Technology Co., Ltd
 * Author: lijing
 * Date: 2021/5/12
 * Description:
 */
@RestController
@RequestMapping("/shop/index")
public class IndexController {

    @Autowired
    private IndexService service;




    /**
     * 获取子菜单
     * @return
     */
    @RequestMapping("/getSubMenu")
    public ActionResult getSubMenu() {
        try {
            List menuList = service.getSubMenu();
            return new ActionResult(ActionResult.ResultStatus.OK, menuList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
        }

    }

    /**
     * 获取商品类型
     * @return
     */
    @RequestMapping("/getGoodsTypes")
    public ActionResult getGoodsTypes() {
        try {
            List<Map<String, Object>> goodsTypeList = service.getGoodsTypes();
            return new ActionResult(ActionResult.ResultStatus.OK, goodsTypeList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ActionResult(ActionResult.ResultStatus.SERVER_ERROR);
        }

    }

}
